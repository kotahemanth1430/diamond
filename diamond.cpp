#include<iostream>
using namespace std;
class Room1{
    public:
    int a=20;
} ;
class Room2: virtual public Room1{
    public:
    int b=10;
};
class Room3:virtual public Room1{
    public:
    int c=30;
};
class Room4:public Room2,public Room3{
    public:
    int d=40;
};
int main()
{
    Room4 obj;
    cout<<"Sum is: "<<obj.a+obj.b+obj.c+obj.d<<endl;
}